﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrrrgh
{
    /// <summary>
    /// 
    /// </summary>
    public class RSession
    {
        //private string _workingDirectory;
        //private string _inputFilename;
        private StringBuilder _rScript;


        //public string WorkingDirectory
        //{
        //    get
        //    {
        //        return _workingDirectory;
        //    }
        //}


        /// <summary>
        /// Create a new empty RSession.
        /// </summary>
        public RSession()
        {
            //_workingDirectory = "c:\\temp\\";
            //_inputFilename = Guid.NewGuid() + ".r";
            _rScript = new StringBuilder();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="rCode"></param>
        public void Append(string rCode)
        {
            _rScript.Append(rCode);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="rCode"></param>
        public void AppendLine(string rCode)
        {
            _rScript.AppendLine(rCode);
        }



        /// <summary>
        /// This will add some R code to export the named variables to an RDA file (name automatically generated).
        /// The name of the file is returned for your use later.
        /// </summary>
        /// <param name="variableNames"></param>
        public string AddRdaExport(params string[] variableNames)
        {
            //http://stackoverflow.com/questions/8345759/how-to-save-a-data-frame-in-r
            //save(foo, file= "data.Rda")
            //load("data.Rda")
            string rdaFilename = Guid.NewGuid() + ".rda";
            _rScript.AppendLine(); //make sure we are clear of the previous command
            _rScript.Append("save(");
            foreach (string s in variableNames)
            {
                _rScript.Append(s + ", ");
            }
            _rScript.Append("file=" + "\"" + rdaFilename + "\"");
            _rScript.AppendLine(")");
            return rdaFilename;
        }



        /// <summary>
        /// This will add some R code to import all the variables in the given RDA file.
        /// We return nothing.
        /// </summary>
        /// <param name="rdaFilename"></param>
        /// <returns></returns>
        public void AddRdaImport(string rdaFilename)
        {
            //http://stackoverflow.com/questions/8345759/how-to-save-a-data-frame-in-r
            //save(foo, file= "data.Rda")
            //load("data.Rda")
            _rScript.AppendLine(); //make sure we are clear of the previous command
            _rScript.Append("load(");
            _rScript.Append("\"" + rdaFilename + "\"");
            _rScript.AppendLine(")");
        }



        /// <summary>
        ///  Runs the R code and cleans up
        /// </summary>
        /// <returns></returns>
        public string Run(string filename)
        {
            return Run(filename, true);
        }



        /// <summary>
        /// Runs the R code, and maybe cleans up
        /// </summary>
        /// <returns></returns>
        public string Run(string filename, bool cleanUp)
        {
            //Write the R script file
            string rCode = _rScript.ToString();
            string inputFileFullPath = Path.GetFullPath(filename); //Path.GetFullPath(_workingDirectory + _inputFilename);
            File.WriteAllText(inputFileFullPath, rCode);

            //Run the script
            string stdOut = "";
            try
            {
                //I don't think we should be hardcoding the R version. Apparently there was some issue with a later version of
                //R, which is why we need to be hard coding. I have added another "approved" version of R. More can be added if
                //we are comfortable with them. (SV 31/08/2023)
                string rfilename="";
                if (File.Exists("C:/Program Files/R/R-3.6.3/bin/x64/rscript.exe"))
                {
                    rfilename = "C:/Program Files/R/R-3.6.3/bin/x64/rscript.exe";
                }
                else if (File.Exists("C:/Program Files/R/R-3.6.1/bin/x64/rscript.exe"))
                {
                    rfilename = "C:/Program Files/R/R-3.6.1/bin/x64/rscript.exe";
                }
                //Set up a process to run R
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = rfilename;
                info.WorkingDirectory = new FileInfo(inputFileFullPath).Directory.FullName;
                info.Arguments = '"' + inputFileFullPath + '"';
                info.RedirectStandardInput = false;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                //Start the process
                using (var proc = new Process())
                {
                    proc.StartInfo = info;
                    proc.Start();
                    stdOut = proc.StandardOutput.ReadToEnd();
                    proc.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("R Script failed: " + stdOut, ex);
            }

            //Cleanup 
            if (cleanUp)
            {
                //Delete the script file
                if (File.Exists(inputFileFullPath))
                    File.Delete(inputFileFullPath);
            }

            //Return
            return stdOut;
        }
    }
}
