﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrrrgh
{
    public class Utils
    {
        /// <summary>
        /// This method makes a comma delimited string of doubles, integers, strings OR dates. This is useful
        /// for making numeric or character vectors for R.
        /// 
        /// N.B. This method is copied directly from the Utils class in DStats (written by Chas). I added
        /// the string and date options, so we can make character vectors too.
        /// </summary>
        /// <param name="dvals"></param>
        /// <param name="ivals"></param>
        /// <returns></returns>
        public static string CommaDelimit(double[] dvals, int[] ivals, string[] svals, DateTime[] dtvals)
        {
            StringBuilder sb = new StringBuilder();
            if (dvals != null && ivals == null && svals == null && dtvals == null)
            {
                for (int i = 0; i < dvals.Length; i++)
                {
                    sb.Append(dvals[i].ToString());
                    if (i < dvals.Length - 1) sb.Append(",");
                }
                return sb.ToString();
            }
            else if (dvals == null && ivals != null && svals == null && dtvals == null)
            {
                for (int i = 0; i < ivals.Length; i++)
                {
                    sb.Append(ivals[i].ToString());
                    if (i < ivals.Length - 1) sb.Append(",");
                }
                return sb.ToString();
            }
            else if (dvals == null && ivals == null && svals != null && dtvals == null)
            {
                for (int i = 0; i < svals.Length; i++)
                {
                    sb.Append("\"" + svals[i] + "\"");
                    if (i < svals.Length - 1) sb.Append(",");
                }
                return sb.ToString();
            }
            else if (dvals == null && ivals == null && svals == null && dtvals != null)
            {
                for (int i = 0; i < dtvals.Length; i++)
                {
                    sb.Append("\"" + dtvals[i].ToString("dd/MM/yyyy") + "\"");
                    if (i < dtvals.Length - 1) sb.Append(",");
                }
                return sb.ToString();
            }

            else throw new Exception("You must specify either doubles or integers or strings or dates but not more than one of those.");
        }
    }
}
